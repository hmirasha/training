<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ExerciseController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::namespace('Api')->group(function() {
    Route::post('login', [UserController::class,'login']);
    Route::post('logout', [UserController::class,'logout']);
    Route::post('register', [UserController::class,'register']);

    Route::post('start-exercise', [ExerciseController::class,'startExercise']);
    Route::post('update-exercise', [ExerciseController::class,'updateExercise']);

 //   Route::post('register', [UserController::class,'register']);
  //  Route::post('register', [UserController::class,'register']);
   // Route::post('register', [UserController::class,'register']);
});
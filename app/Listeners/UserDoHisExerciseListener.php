<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Http;

class UserDoHisExerciseListener
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(object $event): void
    {
        
            $response =  Http::get('/v1/notify', [
                'message' => "Congratulations on completing your daily training dose! You trained for $event->duration_minutes minutes."
            ]);
    }
}

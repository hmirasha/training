<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Exercise extends Model
{
    use HasFactory;

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'exercise_title', 'duration_minutes', 'calories_burned', 'type','user_id','finished_at'
    ];

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }

    

}

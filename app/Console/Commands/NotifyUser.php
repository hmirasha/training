<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class NotifyUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:notify-user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle($trainingMinutes)
    {
        // Logic to check if user has finished a training program more than 30 minutes long
       // $trainingMinutes = 45; // Example training duration

        if ($trainingMinutes > 30) {
            $response = Http::get('/v1/notify', [
                'message' => "Congratulations on completing your daily training dose! You trained for "+ $trainingMinutes+" minutes."
            ]);

            // Handle response as needed
        }
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Models\User;
use App\Models\Exercise;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;

class CheckUserActivity extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'activity:check';
    protected $description = 'Check user activity and send notification if no activity within 10 minutes';


    /**
     * The console command description.
     *
     * @var string
     */
   

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $users = User::all();

        foreach ($users as $user) {
            $last_login_date = Carbon::parse($user->last_login_at)->toDateString();
            $last_started_exercise = Exercise::where('user_id',$user->id)->whereDate('created_at', '>=', $last_login_date)->where("type","Started")->get();
            if (empty($last_started_exercise) && Carbon::now()->diffInMinutes($user->last_login_at) > 10 )
                $message = "Hey there! It looks like you haven't started any activity yet. Why not start training now?";
                $response = Http::post('/v1/notify', [
                    'json' => ['message' => $message]
                ]);
            
        }
        
    }
}

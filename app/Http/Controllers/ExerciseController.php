<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Exercise;
use App\Events\UserDoHisExercise;
use App\Models\User;

class ExerciseController extends Controller
{
    public function startExercise(Request $request)
    {
        $params = $request->all();
        $exercise = Exercise::create([ 
            'exercise_title' => $params['exercise_title'],
            'duration_minutes' => 0,
            'calories_burned' => 0,
            'type' => 'Started',
            'user_id' => $params['user_id'],
            'finished_at'=>\Carbon\Carbon::now()
        ]);
        return response()->json($exercise);
    }

    public function updateExercise(Request $request)
    {
        $params = $request->all();
        $exercise = Exercise::where('id', $params['id'])->latest()->first();
        if ($params['type']=="finish"){
            $exercise->type="finish";
            $exercise->duration_minutes += $params['duration_minutes'];
            $exercise->calories_burned += $params['calories_burned'];
            $user = User::where('id',$exercise->user_id)->first();
            if ($exercise->duration_minutes>30){
                UserDoHisExercise::dispatch($exercise->duration_minutes);
            }
        }else{
            $exercise->type=$params['type'];
            $exercise->duration_minutes += $params['duration_minutes'];
            $exercise->calories_burned += $params['calories_burned'];
        }
        $exercise->save();       
        
        return response()->json($exercise);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Events\UserLogin;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
       /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        if (!Auth::attempt($credentials)) {
            return response()->json([], Response::HTTP_UNAUTHORIZED);
        }

        $user = $request->user();
        $user->last_login_at = now();
        $user->save();
       // UserLogin::dispatch($user);

        return response()->json($user, Response::HTTP_OK);
    }


    public function register(Request $request)
    {
        
        $params = $request->all();
        $user = User::create([ 
            'first_name' => $params['first_name'],
            'last_name' => $params['last_name'],
            'username' => $params['username'],
            'phone' => $params['phone'],
            'gender' => $params['gender'],
            'divice' => $params['divice'],
            'age' => $params['age'],
            'email' => $params['email'],
            'password' => Hash::make($params['password']),
        ]);
        return response()->json($user);
        
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        Auth::guard('web')->logout();
        return response()->json([], Response::HTTP_OK);
    }

}
